""" A script for homogenity analysis of experimental data.

    Usage:
    python statistic_criteria.py --sheet <sheet_name> <path_to_excel>

    Excel file must contain at least three columns named
    _exactly_: "cycle", "flacon" and "RP"

    Known Isuues:
    * Все тесты после Шапиро должны проводиться минимум на двух подвыборках,
      поэтому здесть они делятся по параметру "cycle". Нужно уточнить,
      какие именно подвыборки мы сравниваем в каких случаях и исправить.

      N. B. Тест Фридмана делается на каждый флакон отдельно,
      т. к. требует 3 и юолее подвыборок.

    * Я не до конца понял, что такое ANOVA и как представлять результаты.
      Возможно есть смысл делать этот анализ отдельно от скрипта.

    TODO:
    * Гибкие названия колонок в экселе
    * Тесты (сгенерировать такие выборки, которые бы покрывали все ветки алгоритма)
"""
import argparse
import os

import numpy as np
import pingouin as pg
import scipy.stats as st
from scipy.stats.mstats import gmean

import pandas as pd


parser = argparse.ArgumentParser(
    description="Calculates statistics from input excel"
)
parser.add_argument(
    "path_to_data",
    metavar="PATH",
    type=str,
    help="an excel file with measurments"
)
parser.add_argument(
    "--sheet",
    type=str,
    default="Sheet1",
    help="excel sheet name",
)

## Settings
P_VAL_TH = 0.05
CI_TH = 0.95


def read_data(path, sheet_name):
    df = pd.read_excel(
        path,
        sheet_name=sheet_name,
    )
    return df


def validate(dataframe):
    """ Check if input data is in appropriate format.
    """
    try:
        dataframe["RP"]
    except KeyError:
        raise ValueError("""Invalid format: there must be the "RP" column in the input data.""")
    if len(dataframe.columns) != 3:
        raise ValueError("""Invalid format: there must be exactly 3 columns in the input data.""")


def shapiro(series, pth=P_VAL_TH):
    """ Perform Shapiro normality test on given data `series` with
        p_value threshold `pth`.
    """
    w, p = st.shapiro(series)
    print(f"Perform Shapiro test on {series.shape[0]} samples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def fligner(*series_list, pth=P_VAL_TH):
    """ Perform Fligner-Killeen test
        for equality of variance.

        Fligner’s test tests the null hypothesis
        that all input samples are from populations
        with equal variances. Fligner-Killeen’s test
        is distribution free when populations are identical.
    """
    w, p = st.fligner(*series_list)
    print(f"Perform Fligner-Killeen test on {len(series_list)} subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def friedman(*series_list, pth=P_VAL_TH):
    """ Computes the Friedman test for repeated measurements

        The Friedman test tests the null hypothesis
        that repeated measurements of the same individuals
        have the same distribution. It is often used to test
        for consistency among measurements obtained in different ways.
        For example, if two measurement techniques are used on
        the same set of individuals, the Friedman test
        can be used to determine if the two
        measurement techniques are consistent.


    """
    w, p = st.friedmanchisquare(*series_list)
    print(f"Perform Friedman test on {len(series_list)} subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def student(series_a, series_b, pth=P_VAL_TH):
    """ Calculate the T-test for the means of two independent samples of scores.

        This is a two-sided test for the null hypothesis that 2 independent
        samples have identical average (expected) values.
        This test assumes that the populations have identical variances by default.
    """
    w, p = st.ttest_ind(series_a, series_b)
    print(f"Perform Student test on 2 subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def fischer(*series_list, pth=P_VAL_TH):
    """ Performs a 1-way ANOVA.

    The one-way ANOVA tests the null hypothesis that
    two or more groups have the same population mean.
    The test is applied to samples from two or more groups,
    possibly with differing sizes.
    """
    w, p = st.f_oneway(series_list)
    print(f"Perform Fischer test on {len(series_list)} subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def levene(*series_list, pth=P_VAL_TH):
    """ Perform Levene test for equal variances.

        The Levene test tests the null hypothesis
        that all input samples are from populations
        with equal variances. Levene’s test is
        an alternative to Bartlett’s test bartlett
        in the case where there are significant
        deviations from normality.
    """
    w, p = st.levene(*series_list)
    print(f"Perform Levene test on {len(series_list)} subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def wilcoxon(*series_list, pth=P_VAL_TH):
    """ Calculate the Wilcoxon signed-rank test.

        The Wilcoxon signed-rank test tests the null hypothesis
        that two related paired samples come from the same distribution.
        In particular, it tests whether the distribution of the
        differences x - y is symmetric about zero.
        It is a non-parametric version of the paired T-test.
    """
    w, p = st.wilcoxon(*series_list)
    print(f"Perform Wilcoxon test on {len(series_list)} subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def ansari(*series_list, pth=P_VAL_TH):
    """ Perform the Ansari-Bradley test
        for equal scale parameters

        The Ansari-Bradley test is a non-parametric
        test for the equality of the scale parameter
        of the distributions from which two samples were drawn.
    """
    w, p = st.ansari(*series_list)
    print(f"Perform Ansari-Bradley test on {len(series_list)} subsamples:\nW = {w}\np = {p}")
    if p > pth:
        print("Passed\n")
    else:
        print("Not Passed\n")
    return p > pth


def perform_test_pipeline(dataframe):
    """ 
    """
    passed = False
    subsamples = [
            sdf["RP"]
            for _, sdf in dataframe.groupby("cycle")
        ]
    if shapiro(dataframe["RP"]):
        if len(subsamples) == 2:
            passed = student(*subsamples)
            # alternative
            # passed = fischer(*subsamples)
        else:
            passed = levene(*subsamples)
    else:
        if len(subsamples) == 2:
            passed = wilcoxon(*subsamples) and ansari(*subsamples)
        else:
            passed = fligner(*subsamples)
            for _, sdf in dataframe.groupby("cycle"):
                passed = passed and friedman(
                    [
                        ssdf["RP"]
                        for _, ssdf in sdf.groupby("flacon")
                    ]
                )
    return passed


def perform_anova(df):
    aov = pg.anova(
        dv='RP',
        between=['cycle', 'flacon'],
        data=df,
        detailed=True
    )
    print(f"ANOVA Table:\n{aov}")
    return aov
    

def result(X):
    """ Calculage Gmean + CI
    """
    g_m = gmean(X)
    lci_l, lci_u = st.t.interval(
        CI_TH,   # alpha
        len(X) - 1, # degree
        np.log(g_m),  # mean
        st.sem(np.log(X)) # sem
    )
    print("RP = {:.5f} [{:.5f}, {:.5f}]".format(
        g_m,
        np.exp(lci_l),
        np.exp(lci_u),
    ))
    return g_m, np.exp(lci_l), np.exp(lci_u)

def main():
    args = parser.parse_args()
    df = read_data(
        args.path_to_data,
        args.sheet,
    )
    validate(df)
    if perform_test_pipeline(df):
        result(df["RP"])
    else:
        print("Some test didn't pass, perform ANOVA")
        perform_anova(df)
    

if __name__ == "__main__":
    main()
